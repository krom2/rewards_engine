export default {
  '4': {
    merkle_distributor: "0xbA2F304b62199C979FE9d6C7EEa10b283d49C0f5",
    conditional_distributor: "0x5d014dAA8688DB97B3B65138782920faEBBb32C3",
    erc721_holder: "0xBE1eFff4F86dB8226620126B02Ba2e334d682378"
  },
  '5': {
    merkle_distributor: "0x9E7baB365BcA758681c6ee44bc38BFAf121B6a7d",
    conditional_distributor: "0xb898262910C4A585AbC8be366D6102fc77519ec7",
    erc721_holder: "0xe9589a535cbDDF6aF50a7AC162DEc1dFa1adA188",
    virtual_distributor: "0x2628D5e8fB8D95454ceE66A82Ffc512A5F14D6DC",
    one_time_offchain_tickets: "0x23Fe1Ef7c30c2007559216B2C766A9f10608d61b"
  },
  '137': {
    conditional_distributor: "0x276FE941757C93c4A916B985C59613692e0f551f",
    erc721_holder: "0xd373a0fDf749f8fC28B913014aFc0BE0c17490C6",
    virtual_distributor: "0xf96c3bcF9855D37F60AAB2cDF0C60d149251ef26",
    one_time_offchain_tickets: "0xb3E7F55d98F499c97A1DD9B585D76e10624ca429"
  }
} as {[key: string]: {[key: string]: string}}