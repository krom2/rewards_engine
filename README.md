# @le7el/rewards_engine

Rewards engine includes three contracts for reward distribution: `VirtualDistributor`, `ConditionalDistributor` and `MerkleDistrbutor`. All distributors support ERC20 and ERC1155 as reward tokens.

`VirtualDistributor` allows to allocate staking rewards to NFT metadata. It uses ideas and code from [MasterChefV2](https://github.com/sushiswap/sushiswapV1/blob/1ef0b029c581ba8e254a19b2d7ca397234052ab4/sushiswap/contracts/MasterChefV2.sol) and [TribalChef](https://github.com/fei-protocol/fei-protocol-core/blob/develop/contracts/staking/TribalChief.sol).

`ConditionalDistributor` is used to automate reward distribution based on onchain data supplied by oracle contract.
The current version includes `ERC721Holder` oracle which distribute a fixed reward to holder of ERC721 NFT once and `OneTimeOffchainTickets` which allows issuing arbitary rewards to some address, authorized by off-chain signature of a validator wallet.

`MerkleDistrbutor` is an adapted fork of [@uniswap/merkle-distributor](https://github.com/Uniswap/merkle-distributor).

This version is adopted for web usage (instead of orginal NodeJS) and has the concept of "rounds". As change of round usually implies the change of a root hash, all unclaimed rewards from a previous round could be expired in a next round. 

This version of merkle distributor also has admin controls to declare new rounds, withdraw an unclaimed tokens and pausing / unpausing of the claim process.

# JS

## Installation

    npm install @le7el/rewards_engine

## Usage

    ```js
    import {
        ethers,
        BalanceTree,
        contracts,
        getWeb3Provider,
        MerkleDistributor,
        ConditionalDistributor,
        ERC721Holder,
        OneTimeOffchainTickets,
        VirtualDistributor
    } from "@le7el/rewards_engine"
    ```

Each contract function with exception to `abi`, `bytecode`, `deployedAddress` and `prepareOffchainClaim` supports `web3Provider` and `contractKey` as the last 2 arguments. `web3Provider` should be [ethers.js v5](https://docs.ethers.io/v5/api/providers/) compliant `Web3Provider` or `JsonRpcSigner`. `contractKey` should be a deployed address of the relevant contract. By default `windows.ethereum` will be used as `web3Provider` and canonic deployment of the relevant contract would be used as a `contractKey`.

### ConditionalDistributor

Main interface to claim oracle based rewards.

#### abi() returns (object)

ABI to interact with ConditionalDistributor smart contract.

#### bytecode() returns (string)

Bytecode to deploy your own version of ConditionalDistributor smart contract.

#### deployedAddress(integer chainId) returns (address | null)

Returns canonic deployment of ConditionalDistributor on some network or `null` if it wasn't deployed there.

#### owner() returns (address promise)

Admin address for ConditionalDistributor contract.

#### isClaimed(address oracle, string claim) returns (boolean promise)

Checks if the supplied claim is no longer valid for an `oracle`.

    ```js
    getWeb3Provider()
        .then((provider) => {
            return ERC721Holder.prepareClaim("0x731133e9", "0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F", 0, BigNumber.from("3723987324234324"), provider)
                .then((claim) => ConditionalDistributor.isClaimed(ERC721Holder.deployedAddress(4), claim, provider))
        })
    ```

#### claim(address account, address oracle, bytes4 claimInterface, address rewardToken, integer rewardTokenId, bytes claim) returns (transaction promise)

Claim reward for an `account` in `rewardToken` for the provided `claim` validaded by `oracle`. ERC20 rewards should use 0 as `rewardTokenId`, specific token id is useful for ERC1155 rewards. Rewards can be either minted or transfered from the ConditionalDistributor address, make sure to fill contract beforehands if you use `transfer` claim interfaces. Keep in mind that if you use a proxy contract to manage minting rights for your token (e.g. `MultiMinter`) you should use the address of that proxy as `rewardToken`. The following `claimInterface` are supported:

* Mint ERC20: `0x40c10f19`
* Mint ERC1155: `0x731133e9`
* Transfer ERC1155: `0xd9b67a26`
* Transfer ERC20: `0xffffffff`

To generate `claim` use `prepareOffchainClaim` or `prepareClaim` of the relevant oracle contract (e.g. `ERC721Holdwer`).

    ```js
    getWeb3Provider()
        .then((provider) => {
            return ERC721Holder.prepareClaim("0x731133e9", "0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F", 0, BigNumber.from("3723987324234324"), provider)
                .then((claim) => {
                    return ConditionalDistributor.claim(
                        "0xc4adcF8814a1da13522716A23331Ce4d48A1414d",
                        ERC721Holder.deployedAddress(4), // Rinkeby
                        "0x731133e9",
                        "0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F",
                        0,
                        claim,
                        provider
                    )
                    .then((tr) => provider.waitForTransaction(tr.hash))
                    .then(() => {
                        console.log('reward claimed!')
                    })
                })
        })
    ```

#### batchedClaims(address account, address oracle, bytes4 claimInterface, address rewardToken, integer rewardTokenId, bytes[] claims) returns (transaction promise)

The same as `claim`, but executes several `claims` in a batch. This function expects that `account`, `oracle`, `claimInterface`, `rewardToken` and `rewardTokenId` are the same for all `claims`.

    ```js
    nftIds = ["3723987324234324", "233232", "7973223"]
    claims = nftIds.map((nftId) => ERC721Holder.prepareOffchainClaim("0x731133e9", "0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F", 0, BigNumber.from(nftId)))
    getWeb3Provider()
        .then((provider) => {
            return ConditionalDistributor.claim(
                "0xc4adcF8814a1da13522716A23331Ce4d48A1414d",
                ERC721Holder.deployedAddress(4), // Rinkeby
                "0x731133e9",
                "0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F",
                0,
                claims,
                provider
            )
            .then((tr) => provider.waitForTransaction(tr.hash))
            .then(() => {
                console.log('reward claimed!')
            })
        })
    ```

### ERC721Holder

Used to generate claims and checking rewards for specific NFTs.

#### abi() returns (object)

ABI to interact with ERC721Holder smart contract.

#### bytecode() returns (string)

Bytecode to deploy your own version of ERC721Holder smart contract.

#### deployedAddress(integer chainId) returns (address | null)

Returns canonic deployment of ERC721Holder on some network or `null` if it wasn't deployed there.

#### owner() returns (address promise)

Admin address for ERC721Holder contract.

#### getReward(integer nftId) returns (integer promise)

Checks reward for a specific NFT, keep in mind that it doesn't validate the existance of that NFT so can be false positive.

#### hasClaim(address account, integer nftId) returns (boolean promise)

Checks if a specific account can claim a reward for his NFT.

    ```js
    claim = ERC721Holder.prepareOffchainClaim("0x731133e9", "0x2D3F5666bB1713B13E8fb24F39aA20256Cee2F8F", 0, BigNumber.from("3723987324234324"))
    Promise.all([ERC721Holder.getReward("3723987324234324"), ERC721Holder.hasClaim("0xc4adcF8814a1da13522716A23331Ce4d48A1414d", claim)])
      .then(([reward, valid]) => {
        if (!valid) {
          console.log('invalid claim')
        } else if (reward.eq(BigNumber.from(0))) {
          console.log('no reward')
        } else {
          console.log(reward.toString())
        }
      })
    ```

#### prepareClaim(bytes4 claimInterface, address rewardToken, integer rewardTokenId, integer nftId) returns (bytes promise)

Generate claim of `rewardToken` for specific `nftId` which should be delived by `claimInterface`. Check `ConditionalDistributor.claim` for more details.

#### prepareOffchainClaim(bytes4 claimInterface, address rewardToken, integer rewardTokenId, integer nftId) returns (bytes)

The same as above but synchronous and done offchain with [ethers.js](https://docs.ethers.io/).

### OneTimeOffchainTickets

Allows distribution of fixed rewards based on tickets signed by off-chain validator wallet. Tickets with a lower nonce are invalidated when higher nonce is claimed. Tickets are also invalided if current claimed amount differs from the value when the ticket was generated. It's done to prevent double rewarding with pre-generated, but unclaimed tickets.

The good practise is to always issue a ticket for the full reward at the moment of ticket issuance, if the claimant would use one of older tickets the later ticket would be automatically invalidated, because of correction in a claimed amount, so a claimant would have to generate a new ticket for the remaining pending debt.

#### abi() returns (object)

ABI to interact with ERC721Holder smart contract.

#### bytecode() returns (string)

Bytecode to deploy your own version of ERC721Holder smart contract.

#### deployedAddress(integer chainId) returns (address | null)

Returns canonic deployment of ERC721Holder on some network or `null` if it wasn't deployed there.

#### owner() returns (address promise)

Admin address for OneTimeOffchainTickets contract.

#### getDomainSeparator() returns (string promise)

Part of seed to generate off-chain ticket for the current contract.

#### nextNonce(address account) return (integer promise)

Return next valid nonce to generate next off-chain ticket for the account.

#### claimedAmount(address account) return (integer promise)

Return currently claimed reward amount by account to generate next off-chain ticket for the account.

#### isAllowed(address account, integer amount, integer claimedAmount, integer nonce, string callData) return (boolean promise)

Used to validate ticket signature, but doesn't do the rest of important validations. Most likely you should use `hasClaim` instead, which ensures all the validity constraints.

#### hasClaim(address account, string claim) return (boolean promise)

Checks if specific claim is valid for account.

#### signTicket(Wallet signer, string domainSeparator, address user, integer amount, integer claimedAmount, integer nonce) retirn (string promise)

Use [ethers wallet](https://docs.ethers.io/v5/api/signer/#Wallet) class to sign off-chain ticket. Unless you use node.js on your backend, most likely you'll have to re-implement this function in your backend language.

#### prepareOffchainClaim(string claimInterface, address rewardToken, integer rewardTokenId, address user, integer amount, integer claimedAmount, integer nonce, string ticketSignature) return (string promise)

Prepare off-chain claim for specific amount. `user`, `amount`, `claimedAmount` and `nonce` should be the same as your passed to `signTicket` to generate `ticketSignature` argument. `claimInterface`, `rewardToken` and `rewardTokenId` should be the same as configured by oracle owner.

Full claim example:

    ```
    const TICKET_ORACLE = '0x23Fe1Ef7c30c2007559216B2C766A9f10608d61b'
    const EXP_TOKEN_MINTER = '0xF526929CF357842Eb0aEB76Ff58d3010EF35bB62'
    const ERC1155_MINT_INTERFACE = '0x731133e9'
    const user = '0x30dc9ba4e5e0047848e4291ec448b1576582654e'
    Promise.all([
        nextNonce(user),
        claimedAmount(user),
        getDomainSeparator()
    ]).then(([nonce, claimed, separator]) => {
        const signer = new ethers.Wallet('888fa71d782f31e9d1c952ab74d23a0f8f3f4dc189b8165a94810cf62c805af8') // '0x11169009E2E4956205632177ba1d2F2603342D91'
        return signTicket(signer, separator, user, 100, claimed, nonce)
            .then((ticketSignature) => {
                return prepareOffchainClaim(ERC1155_MINT_INTERFACE, EXP_TOKEN_MINTER, 0, user, 100, claimed, nonce, ticketSignature)
            })
    }).then((claim) => {
        return claim(user, TICKET_ORACLE, ERC1155_MINT_INTERFACE, EXP_TOKEN_MINTER, 0, claim)
    })
    ```

### VirtualDistributor

Used to generate claims and checking rewards for specific NFTs.

#### abi() returns (object)

ABI to interact with VirtualDistributor smart contract.

#### bytecode() returns (string)

Bytecode to deploy your own version of VirtualDistributor smart contract.

#### deployedAddress(integer chainId) returns (address | null)

Returns canonic deployment of VirtualDistributor on some network or `null` if it wasn't deployed there.

#### join(address nftContract, integer nftId) returns (transaction promise)

Join specific NFT to rewards program. Repeatable joins are allowed, in case your NFT level is the same from the last join nothing will happen, will also update your rewards according to your new level.

#### pendingRewards(address nftContract, integer nftId) returns (integer promise)

Returns total amount of accumulated reward tokens for specific NFT. Keep in mind that unlocked rewards are shown as 0 in NFT metadata to prevent abuse on marketplaces.

# Solidity

Install packages

    $ npm install --dev

Install Hardhat

    $ npm install --save-dev hardhat

Launch the local Ethereum client e.g. Ganache:

## Testing

Install local ganache: `npm install --global ganache`

Run it in cli: `ganache`, you may need to change `network_id` for `develop` network in `truffle-config.js`

Run tests with truffle: `yarn test`

## Integration

Run webpack development server: `npx webpack serve --open` or `npm run webpack:watch`

Check `http://localhost:8080/` for Merkle proof generation and validation UX.

Implementation example entrypoints can be found here: `src/index.ts` and `dist/index.html`.

## Verification

To try out Etherscan verification, you first need to deploy a contract to an Ethereum network that's supported by Etherscan, such as Rinkeby.

In this project, copy the `.example` file to a file named `.secret`, and then edit it to fill in the details. Enter your Etherscan API key, your Rinkeby node URL (eg from Infura), and the private key of the account which will send the deployment transaction. With a valid `.secret` file in place, first deploy your contract:

```shell
npx hardhat run --network live_goerli scripts/1_deploy_merkle_distributor.js
npx hardhat run --network live_goerli scripts/2_deploy_conditional_distributor.js
npx hardhat run --network live_goerli scripts/3_deploy_virtual_distributor.js
```

Then, copy the deployment address and paste it in to replace `DEPLOYED_CONTRACT_ADDRESS` in this command:

```shell
npx hardhat verify --network live_goerli DEPLOYED_CONTRACT_ADDRESS ...CONSTRUCTOR_ARGS
```

# Deployments

## Rinkeby

* MerkleDistributor (DAI token) deployed to: `0x1B1d03B59233243cb43844e930a6a1B181077cD9`
* ERC721Holder deployed to: `0xBE1eFff4F86dB8226620126B02Ba2e334d682378`
* ConditionalDistributor deployed to: `0x5d014dAA8688DB97B3B65138782920faEBBb32C3`

## Goerli

* MerkleDistributor (PXP token) deployed to: `0x9E7baB365BcA758681c6ee44bc38BFAf121B6a7d`
* ERC721Holder deployed to: `0xe9589a535cbDDF6aF50a7AC162DEc1dFa1adA188`
* OneTimeOffchainTickets deployed to: `0x23Fe1Ef7c30c2007559216B2C766A9f10608d61b`
* ConditionalDistributor deployed to: `0xb898262910C4A585AbC8be366D6102fc77519ec7`
* VirtualDistributor deployed to: `0x2628D5e8fB8D95454ceE66A82Ffc512A5F14D6DC`

## Polygon

* ERC721Holder deployed to: `0xd373a0fDf749f8fC28B913014aFc0BE0c17490C6`
* OneTimeOffchainTickets deployed to: `0xb3E7F55d98F499c97A1DD9B585D76e10624ca429`
* ConditionalDistributor deployed to: `0x276FE941757C93c4A916B985C59613692e0f551f`
* VirtualDistributor deployed to: `0xf96c3bcF9855D37F60AAB2cDF0C60d149251ef26`
