// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'
const AVATAR_NFT_ADDRESS = "0x7c2a49d8Cb7B29F76185e9163386156359846Cea" // Goerli le7el avatar token

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const VirtualDistributorContract = await ethers.getContractFactory("VirtualDistributor");
  const VirtualDistributor = await VirtualDistributorContract.deploy();
  await VirtualDistributor.deployed();
  console.info("VirtualDistributor deployed to:", VirtualDistributor.address);

  await VirtualDistributor.adminAddPool(AVATAR_NFT_ADDRESS, 1, 10000, ZERO_ADDRESS);

  // Verification
  const NETWORK = "live_goerli"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${VirtualDistributor.address}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});