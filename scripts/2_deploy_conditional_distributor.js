// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'
const ERC1155_MINT_INTERFACE = "0x731133e9";
const AVATAR_NFT_ADDRESS = "0x7c2a49d8Cb7B29F76185e9163386156359846Cea" // Goerli le7el avatar token
const TOKEN_ADDRESS = "0xF526929CF357842Eb0aEB76Ff58d3010EF35bB62"; // Goerli le7el experience token

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  const ERC721HolderContract = await ethers.getContractFactory("ERC721Holder");
  const ERC721Holder = await ERC721HolderContract.deploy(AVATAR_NFT_ADDRESS, ERC1155_MINT_INTERFACE, TOKEN_ADDRESS, 0, 300, ZERO_ADDRESS);
  await ERC721Holder.deployed();
  console.info("ERC721Holder deployed to:", ERC721Holder.address);

  // We get the contract to deploy
  const ConditionalDistributorContract = await ethers.getContractFactory("ConditionalDistributor");
  const ConditionalDistributor = await ConditionalDistributorContract.deploy(ERC721Holder.address);
  await ConditionalDistributor.deployed();
  console.info("ConditionalDistributor deployed to:", ConditionalDistributor.address);

  await ERC721Holder.adminSwitchConsumer(ConditionalDistributor.address, true);

  // Verification
  const NETWORK = "live_goerli"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${ConditionalDistributor.address} ${ERC721Holder.address} \\`)
  console.info(`&& npx hardhat verify --network ${NETWORK} ${ERC721Holder.address} ${AVATAR_NFT_ADDRESS} ${ERC1155_MINT_INTERFACE} ${TOKEN_ADDRESS} 0 300 ${ZERO_ADDRESS}`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});