import chai, { expect } from 'chai'
import { solidity, MockProvider, deployContract } from 'ethereum-waffle'
import { Contract, BigNumber, utils } from 'ethers'

import Distributor from '../artifacts/contracts/VirtualDistributor.sol/VirtualDistributor.json'
import MockRewarder from '../artifacts/contracts/mock/MockRewarder.sol/MockRewarder.json'
import MockERC20 from '../artifacts/contracts/mock/MockERC20.sol/MockERC20.json'
import MockERC721 from '../artifacts/contracts/mock/MockERC721.sol/MockERC721.json'

chai.use(solidity)

const overrides = {
  gasLimit: 9999999,
}

const ZERO_BYTES32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
const ZERO_ADDRESS = '0x0000000000000000000000000000000000000000'

describe('VirtualDistributor', () => {
  const provider = new MockProvider({
    ganacheOptions: {
      hardfork: 'istanbul',
      mnemonic: 'horn horn horn horn horn horn horn horn horn horn horn horn',
      gasLimit: 9999999,
    },
  })

  const wallets = provider.getWallets()
  const [wallet0, wallet1] = wallets

  let token: Contract
  let nft: Contract
  beforeEach('deploy tokens', async () => {
    nft = await deployContract(wallet0, MockERC721, ['Avatar NFT', 'NFT'], overrides)
    token = await deployContract(wallet0, MockERC20, ['Experience', 'EXP', 0], overrides)
  })

  describe('#pool management', () => {
    it('owner can add a new NFT contract to the reward pool', async () => {
      const distributor = await deployContract(wallet0, Distributor, [], overrides)
      await expect(distributor.adminAddPool(nft.address, 1, 1000, ZERO_ADDRESS))
        .to.emit(distributor, 'LogPoolAddition')
        .withArgs(nft.address, 1, 1000, ZERO_ADDRESS)
      expect(await distributor.totalAllocPoint()).to.eq(1)
      expect(await distributor.rewardMultipliers(nft.address)).to.eq(1000)
      const pool = await distributor.poolInfo(nft.address)
      expect(pool.crs).to.eq(await nft.crs())
      expect(pool.project).to.eq(await nft.baseNode())
      expect(pool.rewarder).to.eq(ZERO_ADDRESS)
      expect(pool.virtualTotalSupply).to.eq(0)
      expect(pool.accRewardPerShare).to.eq(0)
      expect(pool.lastRewardBlock).to.eq(await provider.getBlockNumber())
      expect(pool.allocPoint).to.eq(1)

      const nft2 = await deployContract(wallet0, MockERC721, ['Character NFT', 'CHR'], overrides)
      await expect(distributor.adminAddPool(nft2.address, 2, 2000, ZERO_ADDRESS))
        .to.emit(distributor, 'LogPoolAddition')
        .withArgs(nft2.address, 2, 2000, ZERO_ADDRESS)
      expect(await distributor.totalAllocPoint()).to.eq(3)
      expect(await distributor.rewardMultipliers(nft2.address)).to.eq(2000)
    })

    it('only owner can add reward pools', async () => {
      const distributor = await deployContract(wallet0, Distributor, [], overrides)
      await expect(distributor.connect(wallet1).adminAddPool(nft.address, 1, 1000, ZERO_ADDRESS))
        .to.be.revertedWith(
          'Ownable: caller is not the owner'
        )
    })
  })

  describe('#pool usage', () => {
    let distributor: Contract

    beforeEach('deploy', async () => {
      distributor = await deployContract(wallet0, Distributor, [], overrides)
      await distributor.adminAddPool(nft.address, 1, 10000, ZERO_ADDRESS)
    })

    it('NFT owner can join rewards program', async () => {
      await nft.connect(wallet1).mint(wallet1.address, 2)
      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(wallet1.address, nft.address, 2, 1)
    })

    it('cant join rewards program without eligible NFT', async () => {
      const nft2 = await deployContract(wallet0, MockERC721, ['Fake Avatar NFT', 'NFT'], overrides)
      await nft2.connect(wallet1).mint(wallet1.address, 2)
      await expect(distributor.connect(wallet1).join(nft2.address, 2))
        .to.be.revertedWith(
          'invalid multiplier'
        )

      await nft.mint(wallet0.address, 3)
      await expect(distributor.connect(wallet1).join(nft.address, 3))
        .to.be.revertedWith(
          'not an NFT owner'
        )
    })

    it('double joins can dry run or update level', async () => {
      await nft.connect(wallet1).mint(wallet1.address, 2)
      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(wallet1.address, nft.address, 2, 1)
      let pool = await distributor.poolInfo(nft.address)
      expect(pool.virtualTotalSupply).to.eq(1)
      expect(pool.accRewardPerShare).to.eq(0)
      expect(pool.lastRewardBlock).to.eq(await provider.getBlockNumber())
      expect(pool.allocPoint).to.eq(1)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq(0)

      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(wallet1.address, nft.address, 2, 1)
      await distributor.updatePool(nft.address)
      pool = await distributor.poolInfo(nft.address)
      expect(pool.virtualTotalSupply).to.eq(1)
      // 2 blocks 1 reward per share with 1e23 (precision decimals) + 1e18 (token decimals)
      expect(pool.accRewardPerShare).to.eq(BigNumber.from('200000000000000000000000000000000000000000'))
      expect(pool.lastRewardBlock).to.eq(await provider.getBlockNumber())
      expect(pool.allocPoint).to.eq(1)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('2000000000000000000')

      await nft.setDefaultLevel(3)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('3000000000000000000')
      expect(await distributor.connect(wallet1).join(nft.address, 2))
        .to.emit(distributor, 'Dropin')
        .withArgs(wallet1.address, nft.address, 2, 9)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('4000000000000000000')
      await distributor.updatePool(nft.address)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('4999999999999999999')
      pool = await distributor.poolInfo(nft.address)
      expect(pool.virtualTotalSupply).to.eq(9)
      // 4 blocks 1 reward per share plus 1 block 0.11 reward per share because total share amount increased to 10 before the last updatePool call
      expect(pool.accRewardPerShare).to.eq(BigNumber.from('411111111111111111111111111111111111111111'))
      expect(pool.lastRewardBlock).to.eq(await provider.getBlockNumber())
      expect(pool.allocPoint).to.eq(1)
    })

    it('ensure valid allocations for several participants', async () => {
      await nft.connect(wallet1).mint(wallet1.address, 2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await nft.setDefaultLevel(3)
      await nft.connect(wallet1).mint(wallet1.address, 3)
      await distributor.connect(wallet1).join(nft.address, 3)
      await nft.setDefaultLevel(5)
      await nft.connect(wallet1).mint(wallet1.address, 4)
      await distributor.connect(wallet1).join(nft.address, 4)
      await distributor.updatePool(nft.address)
      await nft.setDefaultLevel(2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await distributor.updatePool(nft.address)

      // 3 blocks 100% (3), 3 blocks 10% (0.3), 3 blocks ~2.85% (~0.0857), 1 block ~10.52% (~0.1052)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('3490977443609022556')
      // 3 blocks 0% (0), 3 blocks 90% (2.7), 3 blocks ~25.71% (0.771), 1 block ~23.68% (~0.2368)
      expect(await distributor.pendingRewards(nft.address, 3)).to.eq('3708270676691729323')
      // 6 blocks 0%, 3 blocks ~71.42% (~2.14), 1 block ~65.78% (0.657)
      expect(await distributor.pendingRewards(nft.address, 4)).to.eq('2800751879699248120')
    })
  })

  describe('#harvesting', () => {
    let distributor: Contract
    let rewardToken: Contract

    beforeEach('deploy', async () => {
      rewardToken = await deployContract(wallet0, MockERC20, ["Reward token", "GOLD", 0], overrides)
      let rewarder = await deployContract(wallet0, MockRewarder, [rewardToken.address], overrides)
      distributor = await deployContract(wallet0, Distributor, [], overrides)
      await distributor.adminAddPool(nft.address, 1, 10000, rewarder.address)
      
      await nft.connect(wallet1).mint(wallet1.address, 2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await nft.setDefaultLevel(3)
      await nft.connect(wallet1).mint(wallet1.address, 3)
      await distributor.connect(wallet1).join(nft.address, 3)
      await nft.setDefaultLevel(5)
      await nft.connect(wallet1).mint(wallet1.address, 4)
      await distributor.connect(wallet1).join(nft.address, 4)
      await distributor.updatePool(nft.address)
      await nft.setDefaultLevel(2)
      await distributor.connect(wallet1).join(nft.address, 2)
      await distributor.updatePool(nft.address)
      // Current stacking allocations: NFT 2 - 3.490, NFT 3 - 3.708, NFT 4 - 2.8
    })

    it('NFT owner can claim unlocked rewards', async () => {
      await distributor.connect(wallet1).unlockHarvest(nft.address, 2)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 3)
      await distributor.connect(wallet1).unlockHarvest(nft.address, 4)
      await provider.send("evm_increaseTime", ["86400"])
      await distributor.connect(wallet1).harvest(nft.address, 2)
      expect(await rewardToken.balanceOf(wallet1.address)).to.eq('3912030075187969924') // ~0.42 reward for 4 blocks since 3.490
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq(0)
      await distributor.connect(wallet1).join(nft.address, 2)
      await distributor.updatePool(nft.address)
      expect(await distributor.pendingRewards(nft.address, 2)).to.eq('210526315789473684') // ~0.2105 new reward on NFT, the rest was claimed
      expect(await distributor.pendingRewards(nft.address, 4)).to.eq('6748120300751879699') // ~2.63 reward for 6 blocks since 2.8
    })
  })
})